An application containing an algorithm for text wrapping.
The algorithm accepts 2 arguments: input text and the maximum number of
characters in one line.
The algorithm splits input text into lines in such a way that the length of each line is
less or equal to the given maximum length. It breaks words only if it�s necessary to keep the
required line length. Input text may contain multiple lines.

To run this app, 

1. Build it into an exe file, 

2. Open a command line to the exe folder,

3. Type the exe name, then type the input file name followed by the maximum line length.

An example is:
`text_wrapper_telesoftas_assignment.exe myinputfile.txt 7`