﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace text_wrapper_telesoftas_assignment
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string inputPath = args[0];
                int maxLength = int.Parse(args[1]);

                StringBuilder sb = new StringBuilder();

                string inputText = File.ReadAllText(inputPath);

                string[] lines = inputText.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                foreach (string line in lines)
                {
                    sb.Append(WrapText(line, maxLength));
                }

                File.WriteAllText("output.txt", CleanOutput(sb.ToString()));
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static string WrapText(string line, int maxLength)
        {
            if (line.Length == 0) { 
                return Environment.NewLine; 
            }

            if (line.Length <= maxLength)
            {
                return line + Environment.NewLine;
            }

            string[] words = line.Split(' ');
            StringBuilder allLines = new StringBuilder();
            StringBuilder trimmedLine = new StringBuilder();
            foreach (string word in words)
            {
                if (trimmedLine.Length + 1 + word.Length <= maxLength)
                {
                    trimmedLine.Append(word).Append(" ");
                }
                else
                {
                    string newWord = word;
                    allLines.Append(trimmedLine).Append(Environment.NewLine); ;
                    trimmedLine = new StringBuilder();

                    if (word.Length > maxLength)
                    {
                        while (newWord.Length > maxLength)
                        {
                            trimmedLine.Append(newWord.Substring(0, maxLength)).Append(Environment.NewLine);
                            newWord = newWord.Replace(newWord.Substring(0, maxLength), "");
                        }                        

                        if (!String.IsNullOrEmpty(newWord))
                        {
                            trimmedLine.Append(newWord).Append(" ");
                        }
                    }
                    else
                    {
                        trimmedLine.Append(newWord).Append(" ");
                    }
                }
            }
            if (trimmedLine.Length > 0)
            {
                allLines.Append(trimmedLine);
            }
            allLines.Append(Environment.NewLine);
            return allLines.ToString();
        }

        private static string CleanOutput(string output)
        {
            //i later added this as a shortcut to overcome an issue where empty lines are added, if i had more time, i could have found a more efficient way
            return Regex.Replace(output, @"^\s+$[\r\n]*", string.Empty, RegexOptions.Multiline);
        }     
    }
}
